﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CursorMovement : MonoBehaviour
{
    private Vector3 initialPos;
    private void Awake()
    {
        initialPos = transform.position;
    }

    private void OnMouseDown()
    {
        //Debug.Log("On mouse down!");
        Vector2 MousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 objPosition = Camera.main.ScreenToWorldPoint(MousePosition);
        transform.position = objPosition;
    }

    private void OnMouseDrag()
    {
        //Debug.Log("On mouse drag!");
        Vector2 MousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 objPosition = Camera.main.ScreenToWorldPoint(MousePosition);
        transform.position = objPosition;
    }

    private void OnMouseUp()
    {
        //Debug.Log("On mouse down!");
        Vector2 MousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 objPosition = Camera.main.ScreenToWorldPoint(MousePosition);
        transform.position = objPosition;
    }

    public void ResetCursor()
    {
        Debug.Log("Reset cursor");
        this.enabled = false;
        //transform.position = initialPos;
    }
}
