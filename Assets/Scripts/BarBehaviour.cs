﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarBehaviour : MonoBehaviour
{
    private const int NOACTIVEBARS = 99;
    [SerializeField] private GameObject[] loadedBars;

    private void Start()
    {
    }

    public void IncreaseBar(int amount) // amount is in percentage
    {
        StartCoroutine(IncreaseBarRoutine(amount));
    }

    public void DecreaseBar(int amount) // amount is in percentage
    {
        StartCoroutine(DecreaseBarRoutine(10));
    }

    IEnumerator IncreaseBarRoutine(int amount)
    {
        Debug.Log("Start increase bar!");
        int total = Mathf.RoundToInt((loadedBars.Length * amount) / 100);
        int lastBarActive = GetLastBarActivate() != NOACTIVEBARS ? GetLastBarActivate() : NOACTIVEBARS;

        if (lastBarActive != NOACTIVEBARS)
        {
            for (int i = lastBarActive; i < lastBarActive + total; i++)
            {
                yield return new WaitForSeconds(0.25f);
                loadedBars[i].SetActive(true);
            }
        }
    }

    IEnumerator DecreaseBarRoutine(int amount)
    {
        Debug.Log("Start decrease bar!");
        int total = Mathf.RoundToInt((loadedBars.Length * amount) / 100);
        int lastBarActive = GetLastBarActivate() != NOACTIVEBARS ? GetLastBarActivate() : NOACTIVEBARS;

        if ((lastBarActive != NOACTIVEBARS) && (total <= GetActiveBarsCount()))
        {
            for (int i = lastBarActive; i >= lastBarActive - total; i--)
            {
                yield return new WaitForSeconds(0.25f);
                loadedBars[i].SetActive(false);
            }
        }
        else if ((total > GetActiveBarsCount()))
        {
            total = GetActiveBarsCount();
            for (int i = lastBarActive; i >= lastBarActive - total; i--)
            {
                yield return new WaitForSeconds(0.25f);
                loadedBars[i].SetActive(false);
            }
        }
    }

    private int GetLastBarActivate()
    {
        for (int i = 0; i < loadedBars.Length; i++)
        {
            if (!loadedBars[i].activeInHierarchy)
                return i;
        }
        return NOACTIVEBARS;
    }

    private int GetActiveBarsCount()
    {
        int sum = 0;
        for (int i = 0; i < loadedBars.Length; i++)
        {
            if (loadedBars[i].activeInHierarchy)
                sum += 1;
        }
        return sum;
    }
}
