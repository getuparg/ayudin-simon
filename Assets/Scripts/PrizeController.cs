﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PrizeController : MonoBehaviour
{

    // [SerializeField] private Image prizeImage;
    // [SerializeField] private Sprite prizeOption1;
    // [SerializeField] private Sprite prizeOption2;
    // [SerializeField] private Sprite continueParticipating;
    [SerializeField] private float probability;

    private const int MAX_PRIZES = 26;
    private const int MAX_HOURS = 6;
    private const int MIN_DIFERENCE_FOR_PRIZES = 13;

    private DateTime initialHour;
    private int actualPrizes = 1;
    private bool prizeGiven = false;

    private SavePrizeController savePrizeController;
    private UIManager _uiManager;

    private void Start()
    {
        savePrizeController = FindObjectOfType<SavePrizeController>();
        _uiManager = FindObjectOfType<UIManager>();

        initialHour = DateTime.Now;
        actualPrizes = 1;
        prizeGiven = false;
        //PlayerPrefs.DeleteAll();
        CheckDate();

        //new DateTime(2019, 8, 27, 10, 00, 00);

        // Debug.Log("Diferencia: " + DiferenceBtwNowAndInitialHour());
        //Debug.Log("Diferencia horas: " + DiferenceHoursBtwNowAndInitialHour());
    }

    private void CheckDate()
    {
        DateTime lastTime;
        if (PlayerPrefs.GetString("Date").CompareTo("") == 0)
        {
            lastTime = new DateTime();
        }
        else
        {
            lastTime = DateTime.Parse(PlayerPrefs.GetString("Date"));
        }
        if (lastTime.Day != initialHour.Day)
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("[Prize Controller] - Distinto Dia");
            PlayerPrefs.SetString("Date", initialHour.ToString());
            Debug.Log("[Prize Controller] - Date: " + initialHour);
            PlayerPrefs.SetInt("PrizesGiven", actualPrizes);
            Debug.Log("[Prize Controller] - Prizes: " + actualPrizes);
        }
        else
        {
            if (lastTime.Month == initialHour.Month)
            {
                Debug.Log("[Prize Controller] - Mismo Dia");
                initialHour = lastTime;
                Debug.Log("[Prize Controller] - Date: " + initialHour);
                actualPrizes = PlayerPrefs.GetInt("PrizesGiven");
                Debug.Log("[Prize Controller] - Prizes: " + actualPrizes);
            }
            else
            {
                PlayerPrefs.DeleteAll();
                Debug.Log("[Prize Controller] - Mismo Dia distinto Mes.");
                PlayerPrefs.SetString("Date", initialHour.ToString());
                Debug.Log("[Prize Controller] - Date: " + initialHour);
                PlayerPrefs.SetInt("PrizesGiven", actualPrizes);
                Debug.Log("[Prize Controller] - Prizes: " + actualPrizes);
            }
        }
    }

    private bool NoDiferenceBtwNowAndInitialHour()
    {
        return (((DateTime.Now.Hour - initialHour.Hour) == 0) && ((DateTime.Now.Minute - initialHour.Minute) == 0));
    }

    private int DiferenceBtwNowAndInitialHour()
    {
        TimeSpan diference = DateTime.Now - initialHour;
        float result = (float)diference.TotalMinutes;
        return (int)result;
    }

    private int DiferenceHoursBtwNowAndInitialHour()
    {
        int result = (int)(DiferenceBtwNowAndInitialHour() / 60f);
        return result;
    }

    private void PrizeResult(bool win)
    {
        if (win)
        {
            if (actualPrizes == MAX_PRIZES / 2)
            {
                Debug.Log("Ganaste Cine");
                //prizeImage.sprite = prizeOption2;
                _uiManager.WinCine();
                PrizeInfo prizeToSave = new PrizeInfo(DateTime.Now.ToString(), actualPrizes.ToString(), "Cine");
                savePrizeController.SavePrizeData(prizeToSave);
            }
            else
            {
                Debug.Log("Ganaste Bolsa");
                //prizeImage.sprite = prizeOption1;
                _uiManager.WinBolsa();
                PrizeInfo prizeToSave = new PrizeInfo(DateTime.Now.ToString(), actualPrizes.ToString(), "Bolsa");
                savePrizeController.SavePrizeData(prizeToSave);
            }
            actualPrizes++;
            PlayerPrefs.SetInt("PrizesGiven", actualPrizes);
        }
        else
        {
            Debug.Log("Participa");
            //prizeImage.sprite = continueParticipating;
            _uiManager.NoWin();
        }
    }

    private bool isInTime()
    {
        Debug.Log(DiferenceBtwNowAndInitialHour());
        return (DiferenceBtwNowAndInitialHour() < (MIN_DIFERENCE_FOR_PRIZES * actualPrizes) && DiferenceBtwNowAndInitialHour() >= (MIN_DIFERENCE_FOR_PRIZES * (actualPrizes - 1)));
    }

    public void GetPrize()
    {
        float value = UnityEngine.Random.Range(0f, 1f);
        Debug.Log("Probability: " + value);
        if (isInTime())
        {
            prizeGiven = false;
            Debug.Log("[Prize Controller] - Esta en tiempo");
            if (value > probability)
            {
                Debug.Log("[Prize Controller] - Gano por valor a la prob.");
                if (actualPrizes <= MAX_PRIZES && !prizeGiven)
                {
                    Debug.Log("[Prize Controller] - Premios no alcanzados y premio no dado. - Gano");
                    PrizeResult(true);
                    prizeGiven = true;
                }
                else
                {
                    Debug.Log("[Prize Controller] - Premios alcanzados o premio dado. - Perdio");
                    PrizeResult(false);
                }
            }
            else
            {
                Debug.Log("[Prize Controller] - No gano a la prob. - Perdio");
                PrizeResult(false);
            }
        }
        else if (DiferenceBtwNowAndInitialHour() > (MIN_DIFERENCE_FOR_PRIZES * actualPrizes)) //&& !prizeGiven - sacado
        {
            Debug.Log("[Prize Controller] - Se paso los 13 min y no dio el premio");
            if (actualPrizes <= MAX_PRIZES)
            {
                Debug.Log("[Prize Controller] - Premios no alcanzados - Gano");
                PrizeResult(true);
            }
            else
            {
                Debug.Log("[Prize Controller] - Premios alcanzados - Perdio");
                PrizeResult(false);
            }
        }
        else
        {
            Debug.Log("[Prize Controller] - No esta en tiempo. - Perdio");
            PrizeResult(false);
        }
    }



}
