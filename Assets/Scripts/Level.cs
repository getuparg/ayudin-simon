﻿using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/Level", order = 1)]
public class Level : ScriptableObject
{
    public int level;
    public float timeBtwNotes;
    public int[] notesSecuence;

}