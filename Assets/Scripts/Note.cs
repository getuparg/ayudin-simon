﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Note : MonoBehaviour
{
    public int id;
    public Sprite noteOnSprite;
    public Sprite noteOffSprite;
    public float time;

    enum NoteState { PLAYING, NOTPLAYING }
    private NoteState _state;
    private Image _image;
    private SpriteRenderer _spriteRenderer;
    private AudioSource _noteSound;
    private GameManager _gameManager;
    private void Awake()
    {
        _gameManager = FindObjectOfType<GameManager>();
        _state = NoteState.NOTPLAYING;
        _image = GetComponent<Image>();
        _noteSound = GetComponent<AudioSource>();

        if (_image == null)
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

    }

    public void PlayNote()
    {
        StartCoroutine(PlayNoteRoutine());
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(string.Format("On trigger enter btw {0} and {1}", gameObject.name, other.gameObject.name));
        if (other.CompareTag("Cursor"))
        {
            ChangeState();
            _gameManager.PlayNote(this);
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log(string.Format("On trigger enter btw {0} and {1}", gameObject.name, other.gameObject.name));
        if (other.CompareTag("Cursor"))
        {
            //ChangeState();
        }
    }

    private void ChangeState()
    {
        StartCoroutine(ChangeStateRoutine());
    }

    IEnumerator ChangeStateRoutine()
    {
        switch (_state)
        {
            case NoteState.NOTPLAYING:
                if (_image != null)
                {
                    _image.overrideSprite = noteOnSprite;
                    _noteSound.Play();
                }
                else
                {
                    _spriteRenderer.sprite = noteOnSprite;
                    _noteSound.Play();
                }
                _state = NoteState.PLAYING;

                yield return new WaitForSeconds(_noteSound.clip.length);

                if (_image != null)
                    _image.overrideSprite = noteOffSprite;
                else
                    _spriteRenderer.sprite = noteOffSprite;
                _state = NoteState.NOTPLAYING;

                break;
            case NoteState.PLAYING:
                // if (_image != null)
                //     _image.overrideSprite = noteOffSprite;
                // else
                //     _spriteRenderer.sprite = noteOffSprite;

                // yield return new WaitForSeconds(_noteSound.clip.length);
                // _state = NoteState.NOTPLAYING;
                break;
        }
    }

    IEnumerator PlayNoteRoutine()
    {
        ChangeState();
        yield return new WaitForSeconds(time);
    }

    public void DisableNote()
    {
        this.GetComponent<BoxCollider2D>().enabled = false;
    }


    public void EnableNote()
    {
        this.GetComponent<BoxCollider2D>().enabled = true;
    }

}
