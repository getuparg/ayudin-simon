﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Note[] notes;
    public RandomLevel[] levels;
    public CursorMovement cursor;
    public BarBehaviour barBehaviour;

    [SerializeField] private AudioSource correctSound;
    [SerializeField] private AudioSource incorrectSound;

    [Header("Cursor")]
    [SerializeField] private GameObject cursorPrefab;
    [SerializeField] private Transform cursorParent;
    [SerializeField] private Vector3 cursorInitialPosition;

    private const int MAXRANDOMLEVELSPERLEVEL = 4;

    private int _currentLevel;
    private int _currentNoteCount;
    private int[] _userNotes;
    private UIManager _uiManager;
    private PrizeController _prizeController;
    private int _currentRandomLevel;


    private void Awake()
    {
        _currentLevel = 0;
        _currentNoteCount = 0;
        _currentRandomLevel = Random.Range(0, MAXRANDOMLEVELSPERLEVEL);
        _userNotes = new int[levels[_currentLevel].randomLevels[_currentRandomLevel].notesSecuence.Length];

        _uiManager = FindObjectOfType<UIManager>();
        _prizeController = FindObjectOfType<PrizeController>();

        DisableNotes();
    }

    private void Start()
    {
        //PlaySecuence();
    }
    public void PlaySecuence()
    {
        StartCoroutine(PlaySecuenceRoutine());
    }

    IEnumerator PlaySecuenceRoutine()
    {
        DisableNotes();
        for (int i = 0; i < levels[_currentLevel].randomLevels[_currentRandomLevel].notesSecuence.Length; i++)
        {
            notes[levels[_currentLevel].randomLevels[_currentRandomLevel].notesSecuence[i]].PlayNote();
            yield return new WaitForSeconds(notes[_currentLevel].time);
        }
        EnableNotes();
    }

    public void PlayNote(Note note)
    {
        if (_currentNoteCount < _userNotes.Length)
        {
            _userNotes[_currentNoteCount] = note.id;
            _currentNoteCount++;
            CheckGameState();
        }
    }

    private void CheckGameState()
    {
        if (_currentNoteCount == levels[_currentLevel].randomLevels[_currentRandomLevel].notesSecuence.Length)
        {
            //Game finish
            Debug.Log("Finish");
            if (!CheckUserSecuence())
            {
                //Gameover
                Debug.Log("Gameover");
                _uiManager.StopMusic();
                incorrectSound.Play();
                _prizeController.GetPrize();

            }
            else
            {
                if (_currentLevel < levels.Length - 1)
                {
                    //Are more levels, go to next level
                    Debug.Log("Next level");
                    GoNextLevel();
                }
                else
                {
                    //No more levels, go to prize
                    _uiManager.StopMusic();
                    correctSound.Play();
                    _prizeController.GetPrize();
                }
            }
        }
        else
        {
            //Continue playing
            Debug.Log("Continue");
        }
    }

    private bool CheckUserSecuence()
    {
        for (int i = 0; i < _userNotes.Length; i++)
        {
            if (_userNotes[i] != levels[_currentLevel].randomLevels[_currentRandomLevel].notesSecuence[i])
                return false;
        }
        return true;
    }

    private void GoNextLevel()
    {
        StartCoroutine(GoNextLevelRoutine());
    }

    IEnumerator GoNextLevelRoutine()
    {
        _uiManager.EnableNextLevelPopup();
        DisableNotes();
        _currentLevel++;
        _currentNoteCount = 0;
        _currentRandomLevel = Random.Range(0, MAXRANDOMLEVELSPERLEVEL);
        _userNotes = new int[levels[_currentLevel].randomLevels[_currentRandomLevel].notesSecuence.Length];
        correctSound.Play();
        barBehaviour.IncreaseBar(10);

        ResetCursor();

        yield return new WaitForSeconds(3f);

        _uiManager.DisableNextLevelPopup();
        cursor.enabled = true;

        EnableNotes();
        PlaySecuence();
    }

    private void DisableNotes()
    {
        foreach (Note note in notes)
        {
            note.DisableNote();
        }
    }

    private void EnableNotes()
    {
        foreach (Note note in notes)
        {
            note.EnableNote();
        }
    }

    public void Reset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name.ToString());
    }

    private void ResetCursor()
    {
        Destroy(cursor.gameObject);
        GameObject newCursor = Instantiate(cursorPrefab, cursorInitialPosition, Quaternion.identity, cursorParent);
        cursor = newCursor.GetComponent<CursorMovement>();
    }

}

[System.Serializable]
public class RandomLevel
{
    public Level[] randomLevels;
}
