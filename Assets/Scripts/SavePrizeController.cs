﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.IO;

public class SavePrizeController : MonoBehaviour
{

    public void SavePrizeData(PrizeInfo prize)
    {
        string filePath = Application.persistentDataPath + "/PrizeData.txt";
        StartCoroutine(SavePrizeDataRoutine(filePath, prize));
    }

    IEnumerator SavePrizeDataRoutine(string fileName, PrizeInfo prize)
    {

        if (File.Exists(fileName))
        {
            Debug.Log("Open File");
            var sr = File.Open(fileName, FileMode.Append);

            Byte[] info = new UTF8Encoding(true).GetBytes("---------------------------Premio Nro.: " + prize.prizeID + "---------------------------" + "\n" 
												+ "Premio: " + prize.prizeName + " - Nro: " + prize.prizeID + " - Fecha: " + prize.Date + "\n" + "\n");
            sr.Write(info, 0, info.Length);
            sr.Close();
            //return;
        }
        else
        {
            Debug.Log("Create file: " + fileName);
            var srNew = File.Open(fileName, FileMode.Create);

            Byte[] newInfo = new UTF8Encoding(true).GetBytes("---------------------------Premio Nro.: " + prize.prizeID + "---------------------------" + "\n" 
													+ "Premio: " + prize.prizeName + " - Nro: " + prize.prizeID + " - Fecha: " + prize.Date + "\n" + "\n");

            srNew.Write(newInfo, 0, newInfo.Length);
            srNew.Close();
        }

        yield return null;

    }
}

public class PrizeInfo
{
    public string Date;
    public string prizeID;
    public string prizeName;

	public PrizeInfo(string date, string prizeID, string prizeName)
	{
		this.Date = date;
		this.prizeID = prizeID;
		this.prizeName = prizeName;
	}
}
