﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    [Header("Panels")]
    public GameObject homePanel;
    public GameObject playPanel;
    public GameObject gamemplayBackgroundPanel;
    public GameObject nextLevelpopup;
    public GameObject endPanel;

    [Header("Subpanels")]
    public GameObject noWinpopup;
    public GameObject bolsaPopup;
    public GameObject cinePopup;

    [Header("Gameplay")]
    public GameObject gameplayPanel;
    public AudioSource backgroundMusic;

    private GameManager _gameManager;
    private void Awake()
    {
        _gameManager = FindObjectOfType<GameManager>();
    }

    public void GoPanel(GameObject panelToGo)
    {
        DisablePanels();
        panelToGo.SetActive(true);
    }

    private void DisablePanels()
    {
        homePanel.SetActive(false);
        playPanel.SetActive(false);
        nextLevelpopup.SetActive(false);
        endPanel.SetActive(false);

        noWinpopup.SetActive(false);
        bolsaPopup.SetActive(false);
        cinePopup.SetActive(false);

        gamemplayBackgroundPanel.SetActive(false);
    }

    public void WinBolsa()
    {
        DisablePanels();
        gameplayPanel.SetActive(false);
        endPanel.SetActive(true);
        bolsaPopup.SetActive(true);
    }

    public void WinCine()
    {
        DisablePanels();
        gameplayPanel.SetActive(false);
        endPanel.SetActive(true);
        cinePopup.SetActive(true);
    }

    public void NoWin()
    {
        DisablePanels();
        gameplayPanel.SetActive(false);
        endPanel.SetActive(true);
        noWinpopup.SetActive(true);
    }

    public void EnableNextLevelPopup()
    {
        nextLevelpopup.SetActive(true);
    }

    public void DisableNextLevelPopup()
    {
        nextLevelpopup.SetActive(false);
    }

    public void GoToGame()
    {
        StartCoroutine(GoToGameRoutine());
    }

    IEnumerator GoToGameRoutine()
    {
        backgroundMusic.Play();
        DisablePanels();
        gamemplayBackgroundPanel.SetActive(true);
        gameplayPanel.SetActive(true);

        yield return new WaitForSeconds(3f);

        _gameManager.PlaySecuence();
    }

    public void StopMusic()
    {
        backgroundMusic.Stop();
    }

}
